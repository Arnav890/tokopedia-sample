//
//  SearchViewController.swift
//  Tokopedia_Sample
//
//  Created by Arnav Gupta on 28/05/18.
//  Copyright © 2018 Arnav Gupta. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {

    //MARK: -
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var constraintBtnFilterBottom: NSLayoutConstraint!
    
    //MARK: -
    var refreshControl = UIRefreshControl()
    
    var arrayProductDetail = [ProductDetail]() {
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    var start = 0
    var rows = 10
    var filterModel : Filter?
    var totalItems = 0
    var productReq = ProductRequest()
    
    //MARK: - View delegate
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setup()
        loadItems()
    }
}

//MARK: - Setup
extension SearchViewController {
    
    func setup() {
        
        prepareTheFilterModel()
    }
    
    func loadItems() {
        
        productReq = ProductRequest(from: "samsung", pmin: "10000", pmax: "100000", wholesale: "true", official: "true", fshop: "2", start: start, rows: rows)
        self.api(request: productReq, shouldAddHud: arrayProductDetail.count == 0)
    }
}

//MARK: - Prepare The Default
extension SearchViewController {
    
    func prepareTheFilterModel() {
        
        filterModel = Filter(maxPrice: 100000.0, minPrice: 10000.0, wholeSale: false, arrayShopType: [ShopFilter(name: "Gold Mercent", id: "1"), ShopFilter(name: "Official Store", id: "2")])
    }
    
    func defaultTheVariables() {
        
        start = 0
        rows = 10
        totalItems = 0
        self.productReq.start = "\(start)"
        self.productReq.rows = "\(rows)"
        self.arrayProductDetail.removeAll()
    }
}

//MARK: - IBAction
extension SearchViewController {
    
    @IBAction func btnActionFilter(_ sender: Any) {
        
        let vc = StoryboardScene.Main.instantiateFilterViewController()
        vc.filterModel = filterModel
        self.present(vc)
        
        vc.filterAction = { [weak self] theFilterModel in
            
            self?.productReq.pmin = String(describing: (theFilterModel?.minPrice)*?)
            self?.productReq.pmax = String(describing: (theFilterModel?.maxPrice)*?)
            self?.productReq.wholesale = (theFilterModel?.wholeSale)*? == true ? "true" : "false"
            self?.defaultTheVariables()
            self?.api(request: self?.productReq, shouldAddHud: self?.arrayProductDetail.count == 0)
        }
    }
}

//MARK: - UICollectionView DataSource, Delegate
extension SearchViewController : UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        checkNilData(arr: arrayProductDetail, collectionView: collectionView, contentMode: .center, text: GlobalConstants.StringText.noInformationFound)
        return arrayProductDetail.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier.searchCell.rawValue, for: indexPath) as? SearchCell ?? SearchCell()
        cell.product = arrayProductDetail[indexPath.row]
        
        self.pagination(row: indexPath.row)
        
        return cell
    }
    
    func pagination(row : Int) {
        
        if row == arrayProductDetail.count - 1 {
            if totalItems > arrayProductDetail.count {
                start = rows
                rows += 10
                loadItems()
            }
        }
    }
}

//MARK: - UICollectionView FlowLayout
extension SearchViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = collectionView.frame.size.width/2 - 1.0
        let cellHeight = collectionView.frame.size.height/2 - 1.0
        
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        
        return 1.0
    }
}

//MARK: - HTTPHandler
extension SearchViewController {
    
    func api(request : Any?, shouldAddHud : Bool = true) {
        
        if shouldAddHud { self.addHUD() }
        
        HTTPHandler.handleAPICallFor(requestParam: request,shouldAddHeader : false, responseObj: ProductResponse(), receive: { [weak self] (response, result) in
            
            if shouldAddHud { self?.hideHUD() }
            if (self?.refreshControl.isRefreshing)*? { self?.refreshControl.endRefreshing() }
            
            let success = { () -> () in
                
                if let _response = response as? ProductResponse {
                    
                    guard let _data = _response.data else { return }
                    self?.totalItems = (_response.header?.total_data)*?
                    self?.arrayProductDetail.append(contentsOf: _data)
                }
            }
            result.handleResponse(sender : self, response: response,successFailAlert : false,shouldShowFailMsg : false, successBlock: success)
        })
    }
}
