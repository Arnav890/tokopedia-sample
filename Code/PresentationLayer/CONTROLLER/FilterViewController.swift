//
//  FilterViewController.swift
//  Tokopedia_Sample
//
//  Created by Arnav Gupta on 28/05/18.
//  Copyright © 2018 Arnav Gupta. All rights reserved.
//

import UIKit
import RangeSeekSlider

class FilterViewController: UIViewController {

    //MARK: -
    @IBOutlet weak var rangeSlider: RangeSeekSlider!
    @IBOutlet weak var lblMinPrice: UILabel!
    @IBOutlet weak var lblMaxPrice: UILabel!
    @IBOutlet weak var collectionview: UICollectionView!
    @IBOutlet weak var switchWholesale: UISwitch!
    @IBOutlet weak var constraintCollectionviewHeight: NSLayoutConstraint!
    
    //MARK: -
    var filterModel : Filter?
    var defaultFilterModel : Filter?
    var filterAction :  ((_ filterModel : Filter?)->())?
    
    //MARK: -
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setup()
        setupCollectionview()
        setValueOnFilter()
        setupRangeFilter()
    }
}

//MARK: - Setup
extension FilterViewController {
    
    func setup() {
        self.defaultFilterModel = filterModel
    }
    
    func setValueOnFilter() {
        
        self.lblMaxPrice.text = "\((filterModel?.maxPrice)*?)"
        self.lblMinPrice.text = "\((filterModel?.minPrice)*?)"
        
        self.switchWholesale.isOn = (filterModel?.wholeSale)*?
        
        self.constraintCollectionviewHeight.constant = (self.filterModel?.arrayShopType?.count)*? == 0 ? 0.0 : Height.filterCollectionview
        
        self.collectionview.reloadData()
    }
    
    func setupRangeFilter() {
        
        self.rangeSlider.delegate = self
        self.rangeSlider.handleBorderWidth = 1.0
        self.rangeSlider.handleBorderColor = Theme.shared.theme
        self.rangeSlider.handleDiameter = 24.0
        
        
        self.rangeSlider.minValue = (filterModel?.minPrice)*?
        self.rangeSlider.maxValue = (filterModel?.maxPrice)*?
        
        self.rangeSlider.selectedMinValue = (filterModel?.minPrice)*?
        self.rangeSlider.selectedMaxValue = (filterModel?.maxPrice)*?
    }
    
    func setupCollectionview() {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0.0
        layout.minimumInteritemSpacing = 0.0
        layout.estimatedItemSize = UICollectionViewFlowLayoutAutomaticSize
        layout.scrollDirection = .horizontal
        
        let tagCollectionNib = UINib(nibName: String(describing: FilterShopTypeCell.self), bundle: nil)
        collectionview.register(tagCollectionNib, forCellWithReuseIdentifier: CellIdentifier.filterShopTypeCell.rawValue)
        
        collectionview.collectionViewLayout = layout
    }
}

// MARK: - RangeSeekSliderDelegate
extension FilterViewController: RangeSeekSliderDelegate {
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        
        filterModel?.maxPrice = maxValue
        filterModel?.minPrice = minValue
        
        self.setValueOnFilter()
    }
}

//MARK: - IBAction
extension FilterViewController {
    
    @IBAction func btnActionCancel(_ sender: Any) {
        self.dismissController()
    }
    
    @IBAction func btnActionReset(_ sender: Any) {
        
        filterModel = defaultFilterModel
        setValueOnFilter()
        setupRangeFilter()
        rangeSlider.layoutSubviews()
        
    }
    
    @IBAction func switchValueChanged(_ sender: Any) {
        
        filterModel?.wholeSale = ((sender as? UISwitch)?.isOn)*?
    }
    
    @IBAction func btnActionShopType(_ sender: Any) {
        
        let vc = StoryboardScene.Main.instantiateShopFilterViewController()
        vc.arrayShopFilter = self.filterModel?.arrayShopType
        self.present(vc)
        
        vc.shopFilterAction = { [weak self] arrayShopFilter in
            
            self?.filterModel?.arrayShopType = arrayShopFilter
            self?.setValueOnFilter()
            
        }
    }
    
    @IBAction func btnActionApply(_ sender: Any) {
        
        self.dismiss(animated: true, completion: {
            self.filterAction?(self.filterModel)
        })
    }
}

//MARK: - UICollectionView DataSource, Delegate
extension FilterViewController : UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return (self.filterModel?.arrayShopType?.count)*?
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier.filterShopTypeCell.rawValue, for: indexPath) as? FilterShopTypeCell ?? FilterShopTypeCell()
        cell.shopFilter = self.filterModel?.arrayShopType?[indexPath.row]
        cell.delegate = self
        cell.indexPath = indexPath
        return cell
    }
}

//MARK: - ShopTypeCellDelegate
extension FilterViewController : ShopTypeCellDelegate {
    
    func removeAt(indexPath: IndexPath) {
        
        var _arrayShopType = self.filterModel?.arrayShopType
        _arrayShopType?.remove(at: indexPath.row)
        self.filterModel?.arrayShopType = _arrayShopType
        self.collectionview.reloadData()
        self.constraintCollectionviewHeight.constant = (_arrayShopType?.count)*? == 0 ? 0.0 : Height.filterCollectionview
    }
}
