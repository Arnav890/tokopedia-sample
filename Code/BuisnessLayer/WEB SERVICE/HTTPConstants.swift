//
//  APIConstants.swift
//  Tokopedia_Sample
//
//  Created by Arnav Gupta on 28/05/18.
//  Copyright © 2018 Arnav Gupta. All rights reserved.
//

import Foundation
import Alamofire

let deviceType = GlobalConstants.AppDetails.deviceType
let deviceId = UUID().uuidString

//MARK: - API Method
public enum APIMethod {
    case get
    case post
    case postWithImage
    case put
    
    // Conversion to Alamofire's HTTPMethod
    func associatedHttpMethod() -> HTTPMethod {
        switch self {
        case .get:
            return HTTPMethod.get
        case .post:
            return HTTPMethod.post
        case .put:
            return HTTPMethod.put
        case .postWithImage:
            return HTTPMethod.post
        }
    }
}

//MARK: - API Url
public enum APIUrl : CustomStringConvertible {
    
    case base
    
    public var description : String {
        switch self {
        case .base : return "https://ace.tokopedia.com/search/v2.5/"
        }
    }
}

//MARK: - API Resource Path
public enum APIResourcePath : CustomStringConvertible {
    
    case product
    
    public var description : String {
        
        switch self {
            
        case .product : return "product"
        }
    }
}

//MARK: - API Keys
public enum APIKeys : CustomStringConvertible {
    
    public var description: String { return self.string }
    
    case id
    case name
    case uri
    case image_uri
    case image_uri_700
    case price
    case price_range
    case category_breadcrumb
    case shop
    case wholesale_price
    case condition
    case preorder
    case department_id
    case rating
    case is_featured
    case count_review
    case count_talk
    case count_sold
    case labels
    case badges
    case original_price
    case discount_expired
    case discount_start
    case discount_percentage
    case stock
    case is_gold
    case location
    case reputation_image_uri
    case shop_lucky
    case city
    case count_min
    case count_max
    case title
    case color
    case image_url
    case status
    case header
    case data
    case error_code
    case message
    
    case total_data
    case total_data_no_category
    case additional_params
    case process_time
    case suggestion_instead
    
    var string: String {
        
        switch self {
        
        case .data : return "data"
        case .total_data : return "total_data"
        case .total_data_no_category : return "total_data_no_category"
        case .additional_params : return "additional_params"
        case .process_time : return "process_time"
        case .suggestion_instead : return "suggestion_instead"
        case .error_code : return "error_code"
        case .message : return "message"
        case .status : return "status"
        case .header : return "header"
        case .id : return "id"
        case .name : return "name"
        case .uri : return "uri"
        case .image_uri : return "image_uri"
        case .image_uri_700 : return "image_uri_700"
        case .price : return "price"
        case .price_range : return "price_range"
        case .category_breadcrumb : return "category_breadcrumb"
        case .shop : return "shop"
        case .wholesale_price : return "wholesale_price"
        case .condition : return "condition"
        case .preorder : return "preorder"
        case .department_id : return "department_id"
        case .rating : return "rating"
        case .is_featured : return "is_featured"
        case .count_review : return "count_review"
        case .count_talk : return "count_talk"
        case .count_sold : return "count_sold"
        case .labels : return "labels"
        case .badges : return "badges"
        case .original_price : return "original_price"
        case .discount_expired : return "discount_expired"
        case .discount_start : return "discount_start"
        case .discount_percentage : return "discount_percentage"
        case .stock : return "stock"
        case .is_gold : return "is_gold"
        case .location : return "location"
        case .reputation_image_uri : return "reputation_image_uri"
        case .shop_lucky : return "shop_lucky"
        case .city : return "city"
        case .count_min : return "count_min"
        case .count_max : return "count_max"
        case .title : return "title"
        case .color : return "color"
        case .image_url : return "image_url"
        }
    }
}
