//
//  HTTPResponse.swift
//  Tokopedia_Sample
//
//  Created by Arnav Gupta on 28/05/18.
//  Copyright © 2018 Arnav Gupta. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

let defaultResponseClosure = {() -> () in  }

enum EnumStatusCode : Int {
    
    case success = 200
    case sessionExpired = 401
    case appVersion = 405
    case badRequest = 400
    case paramMissing = 402
    case serverErr = 500
}

//MARK: - API Result
public enum APIResult {
    
    case failure
    case success
    case internetDown
    
    func `is`(success : Int?) -> Bool {
        
        guard let successE = EnumStatusCode(rawValue: success*?) else { return false }
        let isSuccess = successE == .success ? true : false
        return isSuccess
    }
    
    func `is`(versioning : Int?) -> Bool {
        
        guard let versionE = EnumStatusCode(rawValue: versioning*?) else { return false }
        let isSuccess = versionE == .appVersion ? true : false
        return isSuccess
    }
    
    func `is`(sessionExpired : Int?) -> Bool {
        
        guard let sessionExpE = EnumStatusCode(rawValue: sessionExpired*?) else { return false }
        let isSessionExp = sessionExpE == .sessionExpired ? true : false
        return isSessionExp
    }
    
    func `is`(paramMissing : Int?) -> Bool {
        
        guard let statusE = EnumStatusCode(rawValue: paramMissing*?) else { return false }
        let isparamMissing = statusE == .paramMissing ? true : false
        return isparamMissing
    }
    
    public typealias resultClosures = () -> ()
    
    public func handleResponse(sender : UIViewController?, titleOfalert : AlertTitle = AlertTitle(), msgOfalert : AlertMessage = AlertMessage(), response : Any?, successFailAlert : Bool = true,shouldShowFailMsg : Bool = true, failureBlock : @escaping resultClosures = defaultResponseClosure, successBlock : @escaping resultClosures = defaultResponseClosure, noInternetBlock : @escaping resultClosures = defaultResponseClosure, successFailBlock : @escaping resultClosures = defaultResponseClosure) {
        
        DispatchQueue.main.async {
            
            switch self {
                
            case .failure:
                
                if let _ = response as? EmptyResponse {
                    if let msg = msgOfalert.error {
                        if shouldShowFailMsg {
                            Alert().show(title: titleOfalert.error, message: msg, viewController: sender, okAction: {
                                failureBlock()
                            })
                        }
                    }
                    else { failureBlock() }
                }
                else {
                    if shouldShowFailMsg {
                        Alert().show(title: GlobalConstants.AppDetails.appName, message: ErrorMessage.somethingWentWrong, viewController: sender)
                    }
                }
            case .success:
                
                successBlock()
                
            case .internetDown:
               
                if shouldShowFailMsg {
                    if let msg = msgOfalert.noInternet {
                        Alert().show(title: titleOfalert.noInternet, message: msg, viewController: sender, okAction: {
                            noInternetBlock()
                        })
                    }
                }
                else { noInternetBlock() }
            }
        }
    }
}
