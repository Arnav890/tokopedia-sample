//
//  CellIdentifier.swift
//  Tokopedia_Sample
//
//  Created by Arnav Gupta on 28/05/18.
//  Copyright © 2018 Arnav Gupta. All rights reserved.
//

import Foundation

//MARK: - Cell Identifier

enum CellIdentifier : String {
    
    case searchCell = "SearchCell"
    case shopFilterCell = "ShopFilterCell"
    case filterShopTypeCell = "FilterShopTypeCell"
}
