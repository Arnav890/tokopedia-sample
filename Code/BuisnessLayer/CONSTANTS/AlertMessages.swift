//
//  AlertMessages.swift
//  Tokopedia_Sample
//
//  Created by Arnav Gupta on 28/05/18.
//  Copyright © 2018 Arnav Gupta. All rights reserved.
//

import Foundation

public struct AlertMessage {
    
    var error : String?
    var success : String?
    var noInternet  : String?
    
    init(by success : String?,error : String? =  ErrorMessage.somethingWentWrong, noInternet : String? = ErrorMessage.noInternet) {
       
        self.error = error
        self.success = success
        self.noInternet = noInternet
    }
    
    init() {
        
        error = ErrorMessage.somethingWentWrong
        success = nil
        noInternet = ErrorMessage.noInternet
    }
}

public struct AlertTitle {
    
    var error : String?
    var success : String?
    var noInternet  : String?
    
    init(by success : String?,error : String? =  GlobalConstants.AppDetails.appName, noInternet : String? = GlobalConstants.AppDetails.appName) {
       
        self.error = error
        self.success = success
        self.noInternet = noInternet
    }
    
    init() {
        
        self.error = nil
        self.success = nil
        self.noInternet = nil
    }
}

//MARK: - Messages
struct TitleForAlert {
    
    static let success = "Success"
    static let oops = "Oops"
    static let cancel = "Cancel"
    static let error = "Error"
}

//MARK: - Success Message
struct SuccessMessage {
}


//MARK: - Error Message
struct ErrorMessage {
    
    static let somethingWentWrong = "Something went wrong, please try again!"
    static let noInternet = "Please check your internet connection and then try again!"
    static let sessionExpired = "Session Expired.."
}
