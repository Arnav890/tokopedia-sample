//
//  Theme.swift
//  Tokopedia_Sample
//
//  Created by Arnav Gupta on 29/05/18.
//  Copyright © 2018 Arnav Gupta. All rights reserved.
//

import Foundation
import UIKit

class Theme {
    
    static let shared = Theme()    // shared instance
    
    let theme = UIColor(hex: "#5CB54B")
    let grey = UIColor(hex: "#F1F1F1")
    
    let avenirMedium14 = UIFont(name: "Avenir-Medium", size: 14.0)
}
