//
//  GlobalConstants.swift
//  Tokopedia_Sample
//
//  Created by Arnav Gupta on 28/05/18.
//  Copyright © 2018 Arnav Gupta. All rights reserved.
//

import Foundation
import UIKit

//MARK: - App Constants
let zeroAlpha : CGFloat = 0.0
let fullAlpha : CGFloat = 1.0
let animationDuration = 0.5
let animationDelay = 0.0

//MARK: - Strings
let nextLine = "\n"
let emptyString = ""
let entityPredicate = NSPredicate(format: "id != \"\"")

func color(_ rgbColor: Int) -> UIColor{
    return UIColor(
        red:   CGFloat((rgbColor & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbColor & 0x00FF00) >> 8 ) / 255.0,
        blue:  CGFloat((rgbColor & 0x0000FF) >> 0 ) / 255.0,
        alpha: CGFloat(1.0)
    )
}

struct GlobalConstants {
    
    //MARK: - Token
    struct Token {
        
    }
    
    //MARK: - String Constants
    struct StringConstants {
        
        static let euro = " €"
        static let km = " km"
        static let m = " m"
        static let min = " min"
        static let perMin = "/min"
        static let kg = " kg"
        static let lbs = " lbs"
        static let cm = " cm"
        static let inches = " inches"
    }
    
    //MARK: - StringText
    struct StringText {
        
        static let noInformationFound = "No information found, please try again in some time"
    }

    //MARK: - Color
    struct Color {
     
        static let theme = UIColor(hex: "#5CB54B")
        static let grey = UIColor.init(red: 102/255.0, green: 102/255.0, blue: 102/255.0, alpha: 0.4)
    }
    
    //MARK: - Font
    struct Font {
        
        static let avenirHeavy14 = UIFont(name: "Avenir-Heavy", size: 14.0)
    }
    
    //MARK: - App Details
    struct AppDetails {
        
        static let appName = "Tokopedia"
        static let deviceType = "IOS"
        static let website = "http://www.google.com/"
    }
    
    //MARK: - Link
    struct Link {
        
        static let terms = "http://www.google.com".toUrl
        static let privacy = "http://www.google.com".toUrl
        static let help = "http://www.google.com".toUrl
    }
    
    
}

