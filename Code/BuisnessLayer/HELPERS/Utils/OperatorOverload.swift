//
//  OperatorOverload.swift
//  Tokopedia_Sample
//
//  Created by Arnav Gupta on 03/03/17.
//  Copyright © 2018 Arnav Gupta. All rights reserved.
//

import Foundation
import UIKit
protocol OptionalType { init() }

extension String: OptionalType {}
extension Int: OptionalType {}
extension Float: OptionalType {}
extension Double: OptionalType {}
extension CGFloat: OptionalType {}
extension Bool: OptionalType {}
extension UIImage : OptionalType {}
extension IndexPath : OptionalType {}
extension NSNumber : OptionalType {}
extension Date : OptionalType {}


postfix operator *?
postfix func *?<T: OptionalType>( lhs: T?) -> T {
    
    guard let validLhs = lhs else { return T() }
    return validLhs
}
