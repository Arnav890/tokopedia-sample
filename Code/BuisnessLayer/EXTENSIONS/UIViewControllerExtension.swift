//
//  UIViewControllerExtension.swift
//  Tokopedia_Sample
//
//  Created by Arnav Gupta on 28/05/18.
//  Copyright © 2018 Arnav Gupta. All rights reserved.
//

import UIKit

// updated for Swift 3.0
extension UINavigationController {
    
    private func doAfterAnimatingTransition(animated: Bool, completion: @escaping (() -> Void)) {
        if let coordinator = transitionCoordinator, animated {
            coordinator.animate(alongsideTransition: nil, completion: { _ in
                completion()
            })
        } else {
            DispatchQueue.main.async {
                completion()
            }
        }
    }
    
    func pushViewController(viewController: UIViewController, animated: Bool, completion: @escaping (() ->     Void)) {
        pushViewController(viewController, animated: animated)
        doAfterAnimatingTransition(animated: animated, completion: completion)
    }
    
    func popViewController(animated: Bool, completion: @escaping (() -> Void)) {
        popViewController(animated: animated)
        doAfterAnimatingTransition(animated: animated, completion: completion)
    }
    
    func popToRootViewController(animated: Bool, completion: @escaping (() -> Void)) {
        popToRootViewController(animated: animated)
        doAfterAnimatingTransition(animated: animated, completion: completion)
    }
    
    func popToViewController(viewController: UIViewController, animated: Bool, completion: @escaping (() -> Void)) {
        self.popToViewController(viewController, animated: animated)
        doAfterAnimatingTransition(animated: animated, completion: completion)
    }
}

extension UIViewController {
    
    func showToast(message : String, width: CGFloat = 250 , height: CGFloat = 35) {
    
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - width/2, y: self.view.frame.size.height-100, width: width, height: height))
        toastLabel.backgroundColor = UIColor.black
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont.systemFont(ofSize: 12.0)
        self.view.addSubview(toastLabel)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = height == 35 ? (35/2) : 4.0
        toastLabel.clipsToBounds  =  true
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
             toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    func statusBarColor() {
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor = GlobalConstants.Color.theme
        statusBarView.backgroundColor = statusBarColor
        view.addSubview(statusBarView)
    }

    func addTapToHideKeyboard() {
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboardOnViewTap))
        self.view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboardOnViewTap() {
        self.view.endEditing(true)
    }
    
    func dismissToRoot() {
        self.view.window?.rootViewController?.dismiss(animated: false, completion: nil)
    }
    
    func moveBack(animated : Bool = true) {
        _ = self.navigationController?.popViewController(animated: animated)
    }
    
    func pushLikePresent(_ vc: UIViewController) {
        
        let transition:CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromBottom
        self.navigationController!.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func dismissController() {
        dismiss(animated: true, completion: nil)
    }
    
    func addHUD() {
        LoaderManager.shared.addHUD(forView: self.view)
    }
    
    func hideHUD() {
        LoaderManager.shared.hideHUD(forView: self.view)
    }
    
    func push(_ vc: UIViewController, animated: Bool = true) {
        navigationController?.pushViewController(vc, animated: animated)
    }
    
    func present(_ vc: UIViewController) {
        present(vc, animated: true, completion: nil)
    }
    
    var appDelegate : AppDelegate? {
        return UIApplication.shared.delegate as? AppDelegate
    }
    
    func add(child : UIViewController, on frame: CGRect) {
        
        self.addChildViewController(child)
        child.view.frame = frame
        self.view.addSubview(child.view)
        child.didMove(toParentViewController: self)
    }
    
    func remove(child : UIViewController) {
        let vc = child.childViewControllers.last
        vc?.removeFromParentViewController()
        vc?.view.removeFromSuperview()
    }
}

extension UIViewController {
    
    @IBAction func btnActionBack(_ sender: UIButton) {
       moveBack()
    }
}
