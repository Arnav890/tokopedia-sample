//
//  EmptyResponse.swift
//  Bar Finder
//
//  Created by Arnav Gupta on 28/05/18.
//  Copyright © 2018 Arnav Gupta. All rights reserved.
//

import Foundation

struct EmptyResponse  {
    
    var statusCode : Int?
    var message : String?
    
    init(with result : APIResult) {
        switch result {
        case .failure :
            statusCode = 400
            message = ErrorMessage.somethingWentWrong
        case .internetDown :
            statusCode = 300
            message = ErrorMessage.noInternet
        default : break
        }
    }
}
