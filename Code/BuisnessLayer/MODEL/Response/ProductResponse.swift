//
//  ProductResponse.swift
//  Tokopedia_Sample
//
//  Created by Arnav Gupta on 28/05/18.
//  Copyright © 2018 Arnav Gupta. All rights reserved.
//

import Foundation

struct ProductResponse : BaseResponse {
    
    var status : StatusCode?
    var header : Header?
    var data : [ProductDetail]?
}

extension ProductResponse : Unwind {
    
    init(with json : Any?) {
        
        status = StatusCode(with : json <-| APIKeys.status.string)
        header = Header(with : json <-| APIKeys.header.string)
        
        data = ProductDetail.assign(with: json <= APIKeys.data.string)
        
    }
}
