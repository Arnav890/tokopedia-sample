//
//  ProductRequest.swift
//  Tokopedia_Sample
//
//  Created by Arnav Gupta on 28/05/18.
//  Copyright © 2018 Arnav Gupta. All rights reserved.
//

import Foundation

class ProductRequest : BaseRequest,JSONAble {
    
    var q = String()
    var pmin = String()
    var pmax = String()
    var wholesale = String()
    var official = String()
    var fshop = String()
    var start = String()
    var rows = String()
    
    init() {
        
        super.init(with: .product, Method: .get)
        q = String()
        pmin = String()
        pmax = String()
        wholesale = String()
        official = String()
        fshop = String()
        start = String()
        rows = String()
        
    }
    
    init(from q : String?, pmin : String?, pmax : String?, wholesale : String?, official : String?,fshop : String?, start : Int?,rows : Int?) {
        
        super.init(with: .product, Method: .get)
        self.q = q*?
        self.pmin = pmin*?
        self.pmax = pmax*?
        self.wholesale = wholesale*?
        self.official = official*?
        self.fshop = fshop*?
        self.start = String(start*?)*?
        self.rows = String(rows*?)*?
    }
}
