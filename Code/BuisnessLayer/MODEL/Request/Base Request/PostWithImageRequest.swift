//
//  PostWithImageRequest.swift
//  Bar Finder
//
//  Created by Arnav Gupta on 28/05/18.
//  Copyright © 2018 Arnav Gupta. All rights reserved.
//

import Foundation
import UIKit

public class PostWithImageRequest : NSObject {
    
    var image : UIImage?
    var imageParam : String?
    
    init(with img : UIImage, param : String) {
        self.image = img
        self.imageParam = param
    }
}
