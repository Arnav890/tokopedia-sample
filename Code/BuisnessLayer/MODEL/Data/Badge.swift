//
//  Badge.swift
//  Tokopedia_Sample
//
//  Created by Arnav Gupta on 28/05/18.
//  Copyright © 2018 Arnav Gupta. All rights reserved.
//

import Foundation

class Badge : NSObject {
    
    var title = String()
    var image_url = String()
    
    init(with json : Any?) {
        
        title = (json <- APIKeys.title.string)*?
        image_url = (json <- APIKeys.image_url.string)*?
    }
    
    static func assign(with arrayBadge : JSONArray?) -> [Badge] {
        
        guard let _arrayBadge = arrayBadge else { return [] }
        let array = _arrayBadge.map { Badge(with: $0) }
        return array
    }
}
