//
//  ShopFilter.swift
//  Tokopedia_Sample
//
//  Created by Arnav Gupta on 29/05/18.
//  Copyright © 2018 Arnav Gupta. All rights reserved.
//

import Foundation

class ShopFilter {
    
    var name : String?
    var id : String?
    
    init(name : String?, id : String?) {
         self.name = name
        self.id = id
    }
}
