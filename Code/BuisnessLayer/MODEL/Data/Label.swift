//
//  Label.swift
//  Tokopedia_Sample
//
//  Created by Arnav Gupta on 28/05/18.
//  Copyright © 2018 Arnav Gupta. All rights reserved.
//

import Foundation

class Label : NSObject {
    
    var title = String()
    var color = String()
    
    init(with json : Any?) {
        
        super.init()
        title = (json <- APIKeys.title.string)*?
        color = (json <- APIKeys.color.string)*?
    }
    
    static func assign(with arrayLabel : JSONArray?) -> [Label] {
        
        guard let _arrayLabel = arrayLabel else { return [] }
        let array = _arrayLabel.map { Label(with: $0) }
        return array
    }
}
